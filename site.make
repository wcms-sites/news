core = 7.x
api = 2

; addtoany
projects[addtoany][type] = "module"
projects[addtoany][download][type] = "git"
projects[addtoany][download][url] = "https://git.uwaterloo.ca/drupal-org/addtoany.git"
projects[addtoany][download][tag] = "7.x-4.0"
projects[addtoany][subdir] = ""

; uw_fdsu_theme_stories
projects[uw_fdsu_theme_stories][type] = "theme"
projects[uw_fdsu_theme_stories][download][type] = "git"
projects[uw_fdsu_theme_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme_stories.git"
projects[uw_fdsu_theme_stories][download][tag] = "7.x-3.18"
projects[uw_fdsu_theme_stories][subdir] = ""

; uw_stories
projects[uw_stories][type] = "module"
projects[uw_stories][download][type] = "git"
projects[uw_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_stories.git"
projects[uw_stories][download][tag] = "7.x-1.3"
projects[uw_stories][subdir] = ""

; uw_ct_stories
projects[uw_ct_stories][type] = "module"
projects[uw_ct_stories][download][type] = "git"
projects[uw_ct_stories][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_stories.git"
projects[uw_ct_stories][download][tag] = "7.x-2.20"
projects[uw_ct_stories][subdir] = ""

; rabbit_hole
projects[rabbit_hole][type] = "module"
projects[rabbit_hole][download][type] = "git"
projects[rabbit_hole][download][url] = "https://git.drupalcode.org/project/rabbit_hole.git"
projects[rabbit_hole][download][tag] = "7.x-2.25"
projects[rabbit_hole][subdir] = ""

; uw_stories_restful
projects[uw_stories_restful][type] = "module"
projects[uw_stories_restful][download][type] = "git"
projects[uw_stories_restful][download][url] = "https://git.uwaterloo.ca/wcms/uw_stories_restful.git"
projects[uw_stories_restful][download][tag] = "7.x-1.0"
projects[uw_stories_restful][subdir] = ""

; uw_restful_news
projects[uw_restful_news][type] = "module"
projects[uw_restful_news][download][type] = "git"
projects[uw_restful_news][download][url] = "https://git.uwaterloo.ca/wcms/uw_restful_news.git"
projects[uw_restful_news][download][tag] = "7.x-1.1"
projects[uw_restful_news][subdir] = ""

; views_datasource
projects[views_datasource][type] = "module"
projects[views_datasource][download][type] = "git"
projects[views_datasource][download][url] = "https://git.uwaterloo.ca/drupal-org/views_datasource.git"
projects[views_datasource][download][tag] = "7.x-1.0-alpha2"
projects[views_datasource][subdir] = ""

; views_url_alias
projects[views_url_alias][type] = "module"
projects[views_url_alias][download][type] = "git"
projects[views_url_alias][download][url] = "https://git.uwaterloo.ca/drupal-org/views_url_alias.git"
projects[views_url_alias][download][tag] = "7.x-1.0"
projects[views_url_alias][subdir] = ""
